# SmartAlertApp

It is a springboot application. To execute this application run SmartAlertAppApplication.java as java application and then portfolio related events can be access with different http mapping with below urls.
http://localhost:8080/users - this is a post request to push or send or email message to user whenever any update happens in user portfolio.

To run this application run SmartAlertAppApplication.java as a java program and spring boot will start with it's inbuilt tomcat server on port 8080.
Below are the urls for http requets:

GET: http://localhost:8080/users
POST: http://localhost:8080/users
For above post request we need to pass PortfolioRequestModel as Json or Xml in request body
DELETE: http://localhost:8080/users/1
PUT: http://localhost:8080/users/1
For above put request we need to pass PortfolioRequestModel as Json or Xml in request body
PATCH: http://localhost:8080/users/1
For above patch request we need to pass PortfolioRequestModel as Json or Xml in request body

