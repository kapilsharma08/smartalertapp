package com.binck.smartAlertApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmartAlertAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmartAlertAppApplication.class, args);
	}
}
