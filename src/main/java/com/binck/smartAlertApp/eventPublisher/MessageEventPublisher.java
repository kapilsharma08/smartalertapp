package com.binck.smartAlertApp.eventPublisher;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Component;

import com.binck.smartAlertApp.event.MessageEvent;

@Component
public class MessageEventPublisher implements ApplicationEventPublisherAware {
	
	private ApplicationEventPublisher publisher;

	public void setApplicationEventPublisher(ApplicationEventPublisher publisher) {
		this.publisher = publisher;
	}

	public void publish(MessageEvent messageEvent) {
		System.out.println("Publishing event for "+messageEvent.getType());
		publisher.publishEvent(messageEvent);
	}
}