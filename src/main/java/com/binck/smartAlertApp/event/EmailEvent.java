package com.binck.smartAlertApp.event;

import com.binck.smartAlertApp.utility.EventTypeEnum;

public class EmailEvent extends MessageEvent {

	private static final long serialVersionUID = 1L;

	public EmailEvent(Object source) {
		super(source);
	}

	public String getType() {
		return EventTypeEnum.EMAIL.toString();
	}

}
