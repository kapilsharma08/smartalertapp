package com.binck.smartAlertApp.event;

import com.binck.smartAlertApp.utility.EventTypeEnum;

public class PushMessageEvent extends MessageEvent {

	private static final long serialVersionUID = 1L;

	public PushMessageEvent(Object source) {
		super(source);
	}
	
	public String getType() {
		return EventTypeEnum.PUSH.toString();
	}
}
