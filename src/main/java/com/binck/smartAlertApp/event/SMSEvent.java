package com.binck.smartAlertApp.event;

import com.binck.smartAlertApp.utility.EventTypeEnum;

public class SMSEvent extends MessageEvent {

	private static final long serialVersionUID = 1L;

	public SMSEvent(Object source) {
		super(source);
	}

	public String getType() {
		return EventTypeEnum.SMS.toString();
	}
}
