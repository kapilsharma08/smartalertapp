package com.binck.smartAlertApp.event;

import org.springframework.context.ApplicationEvent;

public abstract class MessageEvent extends ApplicationEvent {

	private static final long serialVersionUID = 1L;

	public MessageEvent(Object source) {
		super(source);
	}
	
	public abstract String getType();
}
