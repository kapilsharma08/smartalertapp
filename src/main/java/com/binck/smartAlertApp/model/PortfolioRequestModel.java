package com.binck.smartAlertApp.model;

public class PortfolioRequestModel {
	
	private long portfolioId;
	private String portfolioName;
	
	public long getPortfolioId() {
		return portfolioId;
	}
	public void setPortfolioId(long portfolioId) {
		this.portfolioId = portfolioId;
	}
	public String getPortfolioName() {
		return portfolioName;
	}
	public void setPortfolioName(String portfolioName) {
		this.portfolioName = portfolioName;
	}

}
