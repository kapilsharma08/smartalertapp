package com.binck.smartAlertApp.model;

public class PortfolioResponseModel {
	
	private long portfolioId;
	private boolean portfolioUpdated;
	
	public long getPortfolioId() {
		return portfolioId;
	}
	public void setPortfolioId(long portfolioId) {
		this.portfolioId = portfolioId;
	}
	public boolean isPortfolioUpdated() {
		return portfolioUpdated;
	}
	public void setPortfolioUpdated(boolean portfolioUpdated) {
		this.portfolioUpdated = portfolioUpdated;
	}
}
