package com.binck.smartAlertApp.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.binck.smartAlertApp.model.PortfolioRequestModel;
import com.binck.smartAlertApp.service.PortfolioChangeEventService;

@RestController
@RequestMapping("/users")
public class PortfolioChangeEventController {

	private static final Logger LOGGER = LoggerFactory.getLogger(PortfolioChangeEventController.class);

	@Autowired
	PortfolioChangeEventService portfolioChangeEventService;

	/**
	 * @param portfolioModel
	 * @return this is a port request for updating portfolio which takes portfolio
	 *         data. for successful request we return Http status code OK and rest
	 *         other codes can be sent depending over the requirement
	 */
	@PostMapping(produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity addPortfolio(@RequestBody PortfolioRequestModel portfolioModel) {
		LOGGER.info("create portfolio request execution start");
		portfolioChangeEventService.updatePortfolio();
		LOGGER.info("create portfolio request execution end");
		return new ResponseEntity(HttpStatus.OK);
	}

	@GetMapping
	public ResponseEntity getUserPortfolios() {
		return new ResponseEntity(HttpStatus.OK);
	}

	@PutMapping(path = "{/userId}", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity updateUserPortfolios(@PathVariable String userPortfolioId,
			@RequestBody PortfolioRequestModel portfolioModel) {
		return new ResponseEntity(HttpStatus.OK);
	}

	@PatchMapping(path = "{/userId}", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity partialUpdateUserPortfolios(@PathVariable String userPortfolioId,
			@RequestBody PortfolioRequestModel portfolioModel) {
		return new ResponseEntity(HttpStatus.OK);
	}

	@DeleteMapping
	public ResponseEntity deleteUserPortfolio(@PathVariable String userPortfolioId) {
		return new ResponseEntity(HttpStatus.OK);
	}

}
