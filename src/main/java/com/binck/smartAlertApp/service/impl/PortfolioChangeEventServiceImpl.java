package com.binck.smartAlertApp.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.binck.smartAlertApp.service.PortfolioChangeEventService;

@Service
public class PortfolioChangeEventServiceImpl implements PortfolioChangeEventService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PortfolioChangeEventServiceImpl.class);

	@Override
	public String updatePortfolio() {
		LOGGER.info("portfolio update request");
		// from here we can connect to DB repository but for now for testing we are not connecting to DB side
		return "portfolio updated";
	}
}
