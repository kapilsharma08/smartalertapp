package com.binck.smartAlertApp.eventConsumer;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import com.binck.smartAlertApp.event.MessageEvent;
import com.binck.smartAlertApp.utility.EventTypeEnum;

@Component
public class MessageEventHandler implements ApplicationListener<MessageEvent> {

	public void onApplicationEvent(MessageEvent event) {

		String eventType = event.getType();
		if (EventTypeEnum.EMAIL.toString().equals(eventType)) {
			System.out.println("Sending " + eventType);
		}
		if (EventTypeEnum.SMS.toString().equals(eventType)) {
			System.out.println("Sending " + eventType);
		}
		if (EventTypeEnum.PUSH.toString().equals(eventType)) {
			System.out.println("Sending " + eventType);
		}
	}
}