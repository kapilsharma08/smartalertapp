package com.binck.smartAlertApp.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.binck.smartAlertApp.event.EmailEvent;
import com.binck.smartAlertApp.event.PushMessageEvent;
import com.binck.smartAlertApp.event.SMSEvent;
import com.binck.smartAlertApp.eventPublisher.MessageEventPublisher;

@Aspect
@Component
public class PortfolioChangeAspect {

	@Autowired
	MessageEventPublisher messageEventPublisher;

	@AfterReturning(pointcut = "execution(* com.binck.smartAlertApp.service.impl.PortfolioChangeEventServiceImpl.updatePortfolio()))", returning = "returnValue")
	public void runAfter(JoinPoint joinPoint, String returnValue) throws Throwable {
		messageEventPublisher.publish(new SMSEvent(this));
		messageEventPublisher.publish(new EmailEvent(this));
		messageEventPublisher.publish(new PushMessageEvent(this));
	}
}
